package com.example.medicine_cabinet.helpers

import android.app.Service
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Looper

fun isOnMainThread() = Looper.myLooper() == Looper.getMainLooper()

private var networkMonitor : ConnectivityManager? = null
private var info: NetworkInfo? = null

fun ensureBackgroundThread(callback: () -> Unit) {
    if (isOnMainThread()) {
        Thread {
            callback()
        }.start()
    } else {
        callback()
    }
}
fun isConnected(context: Context): Boolean {
    networkMonitor = context.getSystemService(Service.CONNECTIVITY_SERVICE) as ConnectivityManager
    if(networkMonitor != null){
        info =  networkMonitor!!.activeNetworkInfo
        if (info != null) {
            return info!!.state == NetworkInfo.State.CONNECTED
        }
    }
    return false
}