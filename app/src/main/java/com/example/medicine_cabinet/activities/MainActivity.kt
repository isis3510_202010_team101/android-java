package com.example.medicine_cabinet.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.medicine_cabinet.R
import com.example.medicine_cabinet.database.Medicine_DAO
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_auth.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        Log.d("AUTH", "Current user "+FirebaseAuth.getInstance().currentUser )
        setup()
    }

    val MedicineDB = Medicine_DAO()

    private fun setup(){
        title="Autentication"

        if (FirebaseAuth.getInstance().currentUser!=null){
            FirebaseAuth.getInstance().currentUser?.email?.let { showHome(it) }
        }

        signUpButton.setOnClickListener{
            if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()){
                if (com.example.medicine_cabinet.helpers.isConnected(get_context())) {
                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString()).addOnCompleteListener{
                        MedicineDB.registerCabinet()
                        if (it.isSuccessful){
                            showHome(it.result?.user?.email ?: "")
                        } else {
                            showAlert(it.exception?: Exception())
                        }
                    }
                } else {
                    showMessage("No internet connection, try again later")
                }
            }
        }

        loginButton.setOnClickListener{
            if (emailEditText.text.isNotEmpty() && passwordEditText.text.isNotEmpty()){
                if (com.example.medicine_cabinet.helpers.isConnected(get_context())) {
                    FirebaseAuth.getInstance().signInWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString()).addOnCompleteListener{
                        if (it.isSuccessful){
                            showHome(it.result?.user?.email ?: "")
                        } else {
                            showAlert(it.exception?:Exception())
                        }
                    }
                } else {
                    showMessage("No internet connection, try again later")
                }
            }
        }
    }

    private fun showAlert(error: Exception){

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage("There was an error authenticating the user: " + error.message)
        builder.setPositiveButton("OK", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showMessage(msg: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
        builder.setMessage(msg)
        builder.setPositiveButton("OK", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun showHome(email:String){
        val homeIntent = Intent(this, HomeActivity::class.java).apply {
            putExtra("email", email)
        }
        startActivity(homeIntent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun get_context(): MainActivity {
        return this;
    }

}