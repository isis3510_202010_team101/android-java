package com.example.medicine_cabinet.activities

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.medicine_cabinet.R
import com.example.medicine_cabinet.fragments.CabinetFragment
import com.example.medicine_cabinet.fragments.MapsFragment
import com.example.medicine_cabinet.fragments.SearchFragment
import com.example.medicine_cabinet.fragments.SettingsFragment
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportActionBar?.hide()

        //setup
        val bundle: Bundle? = intent.extras
        val email : String? = bundle?.getString("email")
        val provider : String? = bundle?.getString("provider")
        setup(email ?:"", provider ?: "")

        //fragments
        val cabinetFragment = CabinetFragment()
        val searchFragment = SearchFragment()
        val mapFragment = MapsFragment()
        val settingsFragment = SettingsFragment()

        makeCurrentFragment(searchFragment)

        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.ic_medicine -> makeCurrentFragment(cabinetFragment)
                R.id.ic_search -> makeCurrentFragment(searchFragment)
                R.id.ic_map -> makeCurrentFragment(mapFragment)
                R.id.ic_alarm -> showContact()
            }
            true
        }
    }

    private fun setup(email: String, provider: String) {

        title = "Inicio"

        toolbar.setOnMenuItemClickListener {
            when (it.itemId){
                R.id.logout -> {FirebaseAuth.getInstance().signOut()
                    onBackPressed()
                }
            }
            true
        }
    }

    private fun showContact(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Contact")
        builder.setMessage("Santiago Serrano - sd.serrano@uniandes.edu.co - 3156163463")
        builder.setPositiveButton("OK", null)
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            commit()
        }
}