package com.example.medicine_cabinet.model

data class Medicine(
    val adverseUses: String,
    val desc: String,
    val dose: String,
    val image: String,
    val name: String
){}