package com.example.medicine_cabinet.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.medicine_cabinet.R
import com.example.medicine_cabinet.extensions.RecyclerViewClickListener
import com.example.medicine_cabinet.model.Medicine
import com.example.medicine_cabinet.viewHolders.Search_List_ViewHolder
import kotlinx.android.synthetic.main.search_list_view_holder.view.*

class Search_List_Adapter (val medicines: List<Medicine>, private val listener: RecyclerViewClickListener) : RecyclerView.Adapter<Search_List_ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Search_List_ViewHolder {
        return Search_List_ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.search_list_view_holder,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = medicines.size

    override fun onBindViewHolder(holder: Search_List_ViewHolder, position: Int) {
        val medicine = medicines[position]
        holder.itemView.itemNameSearch.text = medicine.name
        Log.d("AD","Image URL:"+medicine.image)
        Glide.with(holder.itemView.context)
            .load(medicine.image)
            .into(holder.itemView.Medicine_imageView_Search)

        holder.itemView.add_to_list_button.setOnClickListener {
            listener.onRecyclerViewItemClick(it, medicine)
        }
    }
}