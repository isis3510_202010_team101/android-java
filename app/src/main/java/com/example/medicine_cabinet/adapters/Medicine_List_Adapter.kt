package com.example.medicine_cabinet.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.medicine_cabinet.R
import com.example.medicine_cabinet.model.Medicine
import com.example.medicine_cabinet.viewHolders.Medicine_List_ViewHolder
import kotlinx.android.synthetic.main.medicine_list_view_holder.view.*

class Medicine_List_Adapter(val medicines: List<Medicine>) : RecyclerView.Adapter<Medicine_List_ViewHolder>() {

    interface MedicineSelectionRecyclerViewClickListener {
        fun  MedicineClicker(pContext: Context,name: String);
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Medicine_List_ViewHolder {
        return Medicine_List_ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.medicine_list_view_holder,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = medicines.size

    override fun onBindViewHolder(holder: Medicine_List_ViewHolder, position: Int) {

        val medicine = medicines[position]

        Glide.with(holder.itemView.context)
            .load(medicine.image)
            .into(holder.itemView.Medicine_imageView)
        holder.itemView.itemNme.text = medicine.name
    }

}