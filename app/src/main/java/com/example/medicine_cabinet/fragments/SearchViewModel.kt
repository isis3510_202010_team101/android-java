package com.example.medicine_cabinet.fragments

import androidx.lifecycle.ViewModel
import com.example.medicine_cabinet.database.Medicine_DAO
import com.example.medicine_cabinet.model.Medicine

class SearchViewModel: ViewModel() {

    val MedicineDB = Medicine_DAO()

    var medicineList = ArrayList<Medicine>()

    fun getMedicines(callback: ()->Unit){
        MedicineDB.get_medicine_in_search {
            medicineList=it
            callback()
        }
    }

    fun addMedicine(medicine:Medicine,callback: () -> Unit){
        MedicineDB.addToCabinet(medicine) {
            callback()
        }
    }

}