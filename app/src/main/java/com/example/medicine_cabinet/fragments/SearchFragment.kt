package com.example.medicine_cabinet.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.medicine_cabinet.R
import com.example.medicine_cabinet.adapters.Search_List_Adapter
import com.example.medicine_cabinet.database.Medicine_DAO
import com.example.medicine_cabinet.extensions.RecyclerViewClickListener
import com.example.medicine_cabinet.model.Medicine
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.launch

class SearchFragment : Fragment(), RecyclerViewClickListener {

    val MedicineDB = Medicine_DAO()

    private lateinit var viewModel: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SearchViewModel::class.java)

        viewLifecycleOwner.lifecycleScope.launch {
            showMedicine(viewModel.medicineList)
            refreshLayout.setOnRefreshListener {
                viewModel.getMedicines{
                    showMedicine(viewModel.medicineList)
                    refreshLayout.isRefreshing=false
                }
            }
            if (viewModel.medicineList.isEmpty()){
                refreshLayout.isRefreshing=true
                viewModel.getMedicines {
                    showMedicine(viewModel.medicineList)
                    refreshLayout.isRefreshing=false
                }
            } else {
                showMedicine(viewModel.medicineList)
            }
        }

    }

    companion object {
        fun newInstance() = SearchFragment()
    }

    private fun showMedicine(medicines: ArrayList<Medicine>) {
        Search_List.layoutManager = StaggeredGridLayoutManager(2,1)
        Search_List.adapter = Search_List_Adapter(medicines, this)
    }

    override fun onRecyclerViewItemClick(view: View, medicine: Medicine) {
        when(view.id){
            R.id.add_to_list_button -> {
                viewModel.addMedicine(medicine){
                    Toast.makeText(requireContext(), "Medicine added to cabinet", Toast.LENGTH_LONG).show()
                }
            }
        }
    }


}