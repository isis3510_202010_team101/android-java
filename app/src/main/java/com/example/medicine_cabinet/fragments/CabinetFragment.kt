package com.example.medicine_cabinet.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.medicine_cabinet.MedicineDetailActivity
import com.example.medicine_cabinet.R
import com.example.medicine_cabinet.adapters.Medicine_List_Adapter
import com.example.medicine_cabinet.database.Medicine_DAO
import com.example.medicine_cabinet.model.Medicine
import kotlinx.android.synthetic.main.fragment_cabinet.*
import kotlinx.coroutines.launch

class CabinetFragment : Fragment(), Medicine_List_Adapter.MedicineSelectionRecyclerViewClickListener {

    val MedicineDB = Medicine_DAO()

    private lateinit var viewModel: CabinetViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cabinet, container, false)
    }

    companion object {
        fun newInstance(param1: String, param2: String) = CabinetFragment()
        const val INTENT_MEDICINE_KEY = "detail_medicine"
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(CabinetViewModel::class.java)

        viewLifecycleOwner.lifecycleScope.launch {
            showMedicine(viewModel.medicineList)
            if (viewModel.medicineList.isEmpty()){
                viewModel.getMedicine {
                    showMedicine(viewModel.medicineList)
                }
            } else {
                showMedicine(viewModel.medicineList)
            }
        }
    }

    private fun showMedicine(medicines: ArrayList<Medicine>) {
        Medicine_Cabinet_list.layoutManager = StaggeredGridLayoutManager(2,1)
        Medicine_Cabinet_list.adapter = Medicine_List_Adapter(medicines)
    }

    override fun MedicineClicker(pContext: Context ,name: String) {
        showMedicineDetail(pContext, name);
    }

    private fun showMedicineDetail(pContext: Context, MedicineName: String)
    {
        val MedicineDetailIntent = Intent(pContext, MedicineDetailActivity::class.java)
        MedicineDetailIntent.putExtra(INTENT_MEDICINE_KEY, MedicineName);
        startActivity(MedicineDetailIntent);
    }
}