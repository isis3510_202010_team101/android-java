package com.example.medicine_cabinet.fragments

import androidx.lifecycle.ViewModel
import com.example.medicine_cabinet.database.Medicine_DAO
import com.example.medicine_cabinet.model.Medicine

class CabinetViewModel: ViewModel() {

    val MedicineDB = Medicine_DAO()

    var medicineList = ArrayList<Medicine>()

    fun getMedicine(callback: ()->Unit){
        MedicineDB.get_medicine_in_cabinet {
            medicineList=it
            callback()
        }
    }
}