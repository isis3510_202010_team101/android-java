package com.example.medicine_cabinet.database

import android.util.Log
import com.example.medicine_cabinet.model.Medicine
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore

class Medicine_DAO {

    private val db = FirebaseFirestore.getInstance()

    fun get_medicine_in_cabinet(callback: (ArrayList<Medicine>) -> Unit) {
        var listCabinet = ArrayList<Map<String, Object>>()
        var res = ArrayList<Medicine>()

        Log.d("DAO", "HolaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"+FirebaseAuth.getInstance().uid.toString())

        db.collection("userMedicines").document(FirebaseAuth.getInstance().uid.toString())
            .get()
            .addOnSuccessListener {
                if (it.exists()){
                    listCabinet = it.data?.get("medicines") as ArrayList<Map<String, Object>>
                    for (a in listCabinet){
                        res.add(
                            Medicine(
                                a["adverseUses"].toString(),
                                a["desc"].toString(),
                                a["dose"].toString(),
                                a["image"].toString(),
                                a["name"].toString()
                            )
                        )
                    }
                    Log.d("DAO", listCabinet.toString())
                    callback.invoke(res)
                }
            }
    }

    fun get_medicine_in_search(myCallback: (ArrayList<Medicine>) -> Unit) {


        var listMedicine = ArrayList<Map<String,Object>>()
        var res = ArrayList<Medicine>()
        db.collection("globalMedicines").document("HcIpHljlGEdWZPLJDeIH")
            .get()
            .addOnSuccessListener {
                if(it.exists()) {
                    listMedicine = it.data?.get("medicineList") as ArrayList<Map<String, Object>>
                    for (a in listMedicine) {
                        res.add(
                            Medicine(
                                a["adverseUses"].toString(),
                                a["desc"].toString(),
                                a["dose"].toString(),
                                a["image"].toString(),
                                a["name"].toString()
                            )
                        )
                    }
                    myCallback(res)
                }
            }
    }

    fun addToCabinet(medicine:Medicine,myCallback:() ->Unit){
        Log.d("DB", "Medicine added")
        db.collection("userMedicines").document(FirebaseAuth.getInstance().uid.toString())
            .update("medicines", FieldValue.arrayUnion(medicine))
        myCallback()
    }

    fun registerCabinet() {
        db.collection("userMedicines").document(FirebaseAuth.getInstance().uid.toString())
            .set(
                hashMapOf(
                    "name" to "New User",
                    "medicines" to arrayListOf<Medicine>()
                )
            )
    }
}