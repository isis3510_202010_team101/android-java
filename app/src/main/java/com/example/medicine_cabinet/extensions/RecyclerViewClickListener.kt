package com.example.medicine_cabinet.extensions

import android.view.View
import com.example.medicine_cabinet.model.Medicine

interface RecyclerViewClickListener {
    fun onRecyclerViewItemClick(view:View, medicine: Medicine)
}