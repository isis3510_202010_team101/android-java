package com.example.medicine_cabinet.extensions

import com.example.medicine_cabinet.model.Medicine

interface MyCallback {
    fun onCallback(value:  List<Medicine>)
}